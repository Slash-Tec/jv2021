package dataAccess.memory;
import java.util.List;
import models.Identifiable;
public abstract class IndexSortTemplate {
	protected int indexSort(List<Identifiable> data, String id) {
		int start = 0;
		int end = data.size() - 1;
		while (start <= end) {
			int half = (start + end) / 2;	
			int comparison = data.get(half).getId().compareTo(id);		
			if (comparison == 0) {
				return half + 1; 		// Indices base 1
			}
			if (comparison < 0) {
				start = half + 1;
			} 
			else {
				end = half - 1;
			}
		}
		return -(end + 2); 				// Indices base 1
	}
} 